import { Module } from '@nestjs/common';
import { UsersController } from './users/users.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { BlackjackController } from './blackjack/blackjack.controller';
import { PokerController } from './poker/poker.controller';
import { BlackjackModule } from './blackjack/blackjack.module';
import { RouletteModule } from './roulette/roulette.module';
import config from './config/keys';
import { RouletteController } from './roulette/roulette.controller';

@Module({
  imports: [UsersModule, MongooseModule.forRoot(config.mongoURI), BlackjackModule, RouletteModule],
  controllers: [UsersController, BlackjackController, PokerController, RouletteController],
  providers: [],
})
export class AppModule {}
