import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Body,
    Param,
    Res,
  } from '@nestjs/common';
import { CreateBlackjackDto, UpdateBlackjackDto } from './dto/create-blackjack.dto';
import { BlackjackService } from './blackjack.service';
import { Blackjack } from './interfaces/blackjack.interface';

@Controller('api/blackjack')
export class BlackjackController {
    constructor(private readonly blackjackService: BlackjackService) {}
    @Get()
    async findAll(): Promise<Blackjack[]> {
        return this.blackjackService.findAll();
    }
    @Post()
    async startGame(@Body() body): Promise<any>{
        return this.blackjackService.startGame(body.userId, body.bet);
    }
    @Post('hit')
    async hit(@Body() body): Promise<any>{
        return this.blackjackService.hit(body.userId);
    }
    @Post('stand')
    async stand(@Body() body): Promise<any>{
        return this.blackjackService.stand(body.userId);
    }
    @Post('double')
    async double(@Body() body): Promise<any>{
        return this.blackjackService.double(body.userId);
    }
    @Post('split')
    async split(@Body() body): Promise<any>{
        return this.blackjackService.split(body.userId);
    }
    @Post('split/hit')
    async splitHit(@Body() body): Promise<any>{
        return this.blackjackService.splitHit(body.userId, body.handIndex);
    }
    @Post('split/stand')
    async splitStand(@Body() body): Promise<any>{
        return this.blackjackService.splitStand(body.userId, body.handIndex);
    }

}
