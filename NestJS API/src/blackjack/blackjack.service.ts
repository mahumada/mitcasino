import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Blackjack } from './interfaces/blackjack.interface';
import { newDeck, shuffle, newDecks } from '52-deck';
import { getHandValue } from './functions/hand-value.function';
import { getTotalBet } from './functions/get-total-bet.function';
import { User } from '../users/interfaces/user.interface';
import { Bet } from './interfaces/bet.interface';
import { gameBlackjack, gameBlackjackWin, gameDealerBlackjack, gameDealerBusted, gameTie, gameUserBusted, gameUserWon } from './functions/service-functions';

@Injectable()
export class BlackjackService {

    constructor(@InjectModel('Blackjack') private readonly blackjack: Model<Blackjack>, @InjectModel('User') private readonly users: Model<User>) {}

    async findAll(): Promise<Blackjack[]> {
        return await this.blackjack.find();
    }


    async startGame(userId: string, bet: Bet): Promise<any> {
        return await this.blackjack.findOne({userId: userId}).then( async (result) => {
            if (result == null){

                const totalBet = getTotalBet(bet);

                if (totalBet == 0){
                    return {error: "You don't have enough chips to start a game."};
                }
                let user = await this.users.findOne({_id: userId});
                for(let coin in user.blackJackCoins){
                    if(user.blackJackCoins[coin] < bet[coin]){
                        return {error: `You don't have enough ${coin} chips to start a game...`};
                    }
                }
                for(let coin in user.blackJackCoins){
                    user.blackJackCoins[coin] -= bet[coin];
                }

                user.markModified('blackJackCoins');
                await user.save();
                const game = new this.blackjack();
                let deck = shuffle(newDecks(1));
                deck = shuffle(deck);
                const currentHand = [deck[0], deck[1]];
                deck.splice(0, 2);

                if(currentHand[0].value == currentHand[1].value){
                    game.hasPair = true;
                }

                if ((currentHand[0].value + currentHand[1].value) <= 11){
                    game.canDouble = true;
                }

                const dealerHand = [deck[0], deck[1]];
                deck.splice(0, 2);
                const handValue = getHandValue(currentHand);
                const dealerHandValue = getHandValue(dealerHand);
                game.userId = userId;
                game.bet = bet;
                game.currentHand = currentHand;
                game.dealerHand = dealerHand;
                game.currentHandValue = handValue;
                game.dealerHandValue = dealerHandValue;
                game.deck = deck;

                if(dealerHandValue == 21 && handValue == 21){
                    return await gameTie(game, user, deck);
                }else if (handValue == 21){
                    return await gameBlackjackWin(game, user, deck);
                } else if (dealerHandValue == 21){
                    return await gameDealerBlackjack(game, deck);
                }
                await game.save();
                let visibleNewBlackjack = game;
                visibleNewBlackjack.deck = null;
                visibleNewBlackjack.dealerHand[1] = null;
                visibleNewBlackjack.dealerHandValue = getHandValue([visibleNewBlackjack.dealerHand[0]]);
                return visibleNewBlackjack;
            } else {
                let visibleResult = result;
                visibleResult.deck = null;
                visibleResult.dealerHand[1] = null;
                visibleResult.dealerHandValue = getHandValue(visibleResult.dealerHand[0]);
                return result;
            }
        });
    }

    async split(userId: string): Promise<any> {
        return await this.blackjack.findOne({userId}).then( async (result) => {
            if (result == null){
                return {error: "This user doesn't have a game yet"};
            } else {
                const game = await this.blackjack.findOne({userId});
                if (game.hasPair){
                    const user = await this.users.findOne({_id: userId});
                    for(let coin in user.blackJackCoins){
                        if(user.blackJackCoins[coin] < game.bet[coin]){
                            return {error: `You don't have enough ${coin} chips to split...`};
                        }
                    }
                    for(let coin in user.blackJackCoins){
                        user.blackJackCoins[coin] -= game.bet[coin];
                    }
                    for(let coin in game.bet){
                        game.bet[coin] = game.bet[coin] * 2;
                    }
                    let currentHand = game.currentHand;
                    game.handIsBusted = [false, false];
                    game.handWon = [false, false];
                    game.handHasBlackjack = [false, false];
                    game.handStand = [false, false];
                    game.handTie = [false, false];
                    game.currentHandValue = null;
                    game.currentHand = [[currentHand[0], game.deck[0]],[currentHand[1], game.deck[1]]];
                    game.splitHandValues = [0, 0];
                    game.deck.splice(0, 2);
                    for(let i = 0; i < game.currentHand.length; i++){
                        game.splitHandValues[i] = getHandValue(game.currentHand[i]);
                        if(game.splitHandValues[i] == 21){
                            game.handHasBlackjack[i] = true;
                            game.handStand[i] = true;
                        }
                    }
                    await game.save();
                    let visibleNewBlackjack = game;
                    visibleNewBlackjack.dealerHand[1] = null;
                    visibleNewBlackjack.dealerHandValue = getHandValue([visibleNewBlackjack.dealerHand[0]]);
                    visibleNewBlackjack.deck = null;
                    return visibleNewBlackjack;
                }else {
                    return {error: "You can't split a hand that doesn't have a pair"};
                }
            }
        });
    }

    async double(userId: string): Promise<any> {
        return await this.blackjack.findOne({userId}).then( async (result) => {
            if (result == null){
                return {error: "This user doesn't have a game yet"};
            } else {
                let game = await this.blackjack.findOne({userId});
                if(game.canDouble){
                    let user = await this.users.findOne({_id: userId});
                    let userCoins = user.blackJackCoins;
                    const deck = game.deck;
                    let currentHand = game.currentHand;
                    currentHand.push(deck[0]);
                    deck.splice(0, 1);
                    const handValue = getHandValue(currentHand);
                    game.currentHandValue = handValue;
                    if (handValue > 21){
                        return await gameUserBusted(game, deck);
                    }else if (handValue == 21){
                        return await gameBlackjack(game, user, deck);
                    }
                    else {
                        let dealerHand = game.dealerHand;
                        let currentHandValue = game.currentHandValue;
                        let dealerHandValue = getHandValue(dealerHand);
                        while (dealerHandValue <= 17 || currentHandValue > dealerHandValue){ // Hasta que llegue a 17 O Gane al player
                            dealerHand.push(deck[0]);
                            deck.splice(0, 1);
                            dealerHandValue = getHandValue(dealerHand);
                        }
                        if (dealerHandValue > 21){
                            return await gameDealerBusted(game, userCoins, dealerHandValue, user);
                        } else if (dealerHandValue == 21){
                            return await gameDealerBlackjack(game, deck);
                        } else if (currentHandValue > dealerHandValue){
                            return await gameUserWon(game, user, deck);
                        } else{
                            game.dealerWon = true;
                            game.deck = deck;
                            await game.save();
                            let visibleResult = game;
                            visibleResult.deck = null;
                            return visibleResult;
                        }
                    }
                } else{
                    return {error: "You can't double this hand"};
                }
            }

        });
    }

    async splitDouble(userId: string, handIndex: number): Promise<any> {
        return await this.blackjack.findOne({userId}).then( async (result) => {
            if (result == null){
                return {error: "This user doesn't have a game yet"};
            } else {
                let game = await this.blackjack.findOne({userId});
                let user = await this.users.findOne({_id: userId});
                if(!game.canDouble){
                    return {error: "You can't double this hand"};
                } else {
                    let userCoins = user.blackJackCoins;
                    const deck = game.deck;
                    let currentHand = game.currentHand[handIndex];
                }
            }
        });
    }

    async hit(userId: string): Promise<any> {
        return await this.blackjack.findOne({userId}).then( async (result) => {
            if (result == null){
                return {error: "This user doesn't have a game yet"};
            } else {
                const game = await this.blackjack.findOne({userId});
                const deck = game.deck;
                let currentHand = game.currentHand;
                currentHand.push(deck[0]);
                deck.splice(0, 1);
                const handValue = getHandValue(currentHand);
                let user = await this.users.findOne({_id: userId});
                let userCoins = user.blackJackCoins;
                game.currentHandValue = handValue;
                if (handValue > 21){
                    return await gameUserBusted(game, deck);
                } else if (handValue == 21){
                    return await gameBlackjack(game, user, deck);
                }
                else {
                    game.deck = deck;
                    await game.save();
                    let visibleResult = game;
                    visibleResult.deck = null;
                    visibleResult.dealerHand[1] = null;
                    visibleResult.dealerHandValue = getHandValue(visibleResult.dealerHand[0]);
                    return visibleResult;
                }
            }
        });
    }

    async splitHit(userId: string, handIndex: number): Promise<any> {
        return await this.blackjack.findOne({userId}).then( async (result) => {
            if (result == null){
                return {error: "This user doesn't have a game yet"};
            } else {
                let user = await this.users.findOne({_id: userId});
                let userCoins = user.blackJackCoins;
                let game = await this.blackjack.findOne({userId});
                if(game.handHasBlackjack[handIndex]){
                    return {error: "This hand is busted"};
                }else{
                    let deck = game.deck;
                    if (game.handHasBlackjack[handIndex]){
                        return {error: "This hand has blackjack"};
                    }
                    if (game.handIsBusted[handIndex]){
                        return {error: "This hand is busted"};
                    }
                    let currentHand = game.currentHand[handIndex];
                    currentHand.push(deck[0]);
                    game.currentHand[handIndex] = currentHand;
                    let handValue = getHandValue(game.currentHand[handIndex]);
                    deck.splice(0, 1);
                    game.splitHandValues[handIndex] = handValue;
                    await game.save();
                    if (handValue > 21){
                        game.handIsBusted[handIndex] = true;
                        game.handStand[handIndex] = true;
                        game.deck = deck;
                        await game.save();
                        let visibleResult = game;
                        visibleResult.dealerHand[1] = null;
                        visibleResult.deck = null;
                        if(game.handIsBusted[0] && game.handIsBusted[1]){
                            await this.splitStand(userId, handIndex);
                        }
                        return visibleResult;
                    } else if (handValue == 21){
                        game.handHasBlackjack[handIndex] = true;
                        game.deck = deck;
                        await game.save();
                        let visibleResult = game;
                        visibleResult.dealerHand[1] = null;
                        visibleResult.deck = null;
                        if(game.handHasBlackjack[0] && game.handHasBlackjack[1]){
                            await game.delete();
                        }
                        return visibleResult;
                    }
                    else {
                        game.deck = deck;
                        await game.save();
                        let visibleResult = game;
                        visibleResult.dealerHand[1] = null;
                        visibleResult.deck = null;
                        return visibleResult;
                    }
                }
            }
        });
    }


    async stand(userId: string): Promise<any> {
        return await this.blackjack.findOne({userId}).then( async (result) => {
            if (result == null){ // No hay apuestas
                return {error: "This user doesn't have a game yet"};
            } else {
                const game = await this.blackjack.findOne({userId});
                const deck = game.deck;
                let dealerHand = game.dealerHand;
                let currentHandValue = game.currentHandValue;
                let dealerHandValue = game.dealerHandValue;
                while (dealerHandValue <= 17 || currentHandValue > dealerHandValue){ // Hasta que llegue a 17 O Gane al player
                    dealerHand.push(deck[0]);
                    deck.splice(0, 1);
                    dealerHandValue = getHandValue(dealerHand);
                }
                let user = await this.users.findOne({_id: userId});
                let userCoins = user.blackJackCoins;
                if(dealerHandValue > 21 || currentHandValue > dealerHandValue){ // El dealer se pasa O Pierde en valor
                    return await gameDealerBusted(game, userCoins, dealerHandValue, user);
                } else {
                    game.dealerHandValue = dealerHandValue;
                    await game.save();
                    user.blackJackCoins = userCoins;
                    user.markModified('blackJackCoins');
                    await user.save();
                    let visibleResult = game;
                    visibleResult.deck = null;
                    await game.delete();
                    return visibleResult;
                }
            }
        });
    }

    async splitStand(userId: string, handIndex: number): Promise<any> {
        return await this.blackjack.findOne({userId}).then( async (result) => {
            if (result == null){ // No hay apuestas
                return {error: "This user doesn't have a game yet"};
            } else {
                const game = await this.blackjack.findOne({userId});
                let user = await this.users.findOne({_id: userId});
                let userCoins = user.blackJackCoins;
                const deck = game.deck;
                let dealerHand = game.dealerHand;
                let currentHandValue = game.splitHandValues[handIndex];
                game.handStand[handIndex] = true;
                if(game.handStand[0] && game.handStand[1]){
                    let dealerHandValue = game.dealerHandValue;
                    while (dealerHandValue <= 17 || currentHandValue > dealerHandValue){ // Hasta que llegue a 17 O Gane al player
                        dealerHand.push(deck[0]);
                        deck.splice(0, 1);
                        dealerHandValue = getHandValue(dealerHand);
                    }
                    for(let i = 0; i < 2 ; i++){
                        if(!game.handIsBusted[i]){
                            let handValue = Number(game.splitHandValues[i]);
                            if(dealerHandValue > 21 || handValue > dealerHandValue){ // El dealer se pasa O Pierde en valor
                                if(dealerHandValue > 21){
                                    game.dealerIsBusted = true;
                                }
                                for(let coin in userCoins){
                                    userCoins[coin] += game.bet[coin];
                                }
                            } else if (dealerHandValue == 21){
                                if(currentHandValue == 21){
                                    for(let coin in userCoins){
                                        userCoins[coin] += game.bet[coin] / 2;
                                    }
                                    game.handTie[i] = true;
                                } else if (currentHandValue > 21){
                                    game.dealerWon = true;
                                }
                            }
                        }
                    }
                    game.dealerHandValue = dealerHandValue;
                    await game.save();
                    user.blackJackCoins = userCoins;
                    user.markModified('blackJackCoins');
                    await user.save();
                    let visibleResult = game;
                    visibleResult.deck = null;
                    await game.delete();
                    return visibleResult;
                }
            }
        });
    }

}
