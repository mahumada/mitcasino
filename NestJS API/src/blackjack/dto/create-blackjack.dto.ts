export class CreateBlackjackDto {
    userId: string;
    bet: {};
    currentHand: any[];
    hasPair: boolean;
    canDouble: boolean;
    hasSplited: boolean;
    dealerHand: any[];
    deck: any[];
    currentHandValue: any;
    splitHandValues: any[];
    splitBlackjacks: any[];
    handHasBlackjack: any[];
    handCanDouble: any[];
    handStand: any[];
    handTie: any[];
    userHasBlackjack: boolean;
    dealerHasBlackjack: boolean;
    userIsBusted: boolean;
    dealerWon: boolean;
    userWon: boolean;
    tie: boolean;
    handWon: any[];
    handIsBusted: any[];
    dealerIsBusted: boolean;
}

export class UpdateBlackjackDto {
    bet: {};
    currentHand: any[];
    dealerHand: any[];
    deck: any[];
    currentHandValue: any;
    splitHandValues: any[];
    handHasBlackjack: any[];
    handCanDouble: any[];
    handStand: any[];
    handTie: any[];
    userHasBlackjack: boolean;
    hasSplited: boolean;
    dealerHasBlackjack: boolean;
    dealerWon: boolean;
    userWon: boolean;
    userIsBusted: boolean;
    handWon: any[];
    handIsBusted: any[];
    dealerIsBusted: boolean;
}