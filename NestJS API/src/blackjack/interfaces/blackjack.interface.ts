export interface Blackjack {
    id?: string;
    userId: string;
    bet?: {
        one: number,
        five: number,
        ten: number,
        twentyfive: number,
        fifty: number,
        hundred: number,
        twohundred: number,
        fivehundred: number,
        thousand: number
    };
    currentHand?: any[];
    hasPair?: boolean;
    canDouble?: boolean;
    hasSplited?: boolean;
    dealerHand?: any[];
    deck?: any[];
    currentHandValue?: number;
    dealerHandValue?: number;
    splitHandValues?: any[];
    handHasBlackjack?: any[];
    handStand?: any[];
    handWon?: any[];
    handCanDouble?: any[];
    handTie?: any[];
    userIsBusted?: boolean;
    userHasBlackjack?: boolean;
    dealerHasBlackjack?: boolean;
    handIsBusted?: any[];
    userWon?: boolean;
    tie?: boolean;
    dealerWon?: boolean;
    dealerIsBusted?: boolean;
}