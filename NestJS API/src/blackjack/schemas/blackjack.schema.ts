import * as mongoose from 'mongoose';

export const BlackjackShema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    bet: {
        one: Number,
        five: Number,
        ten: Number,
        twentyfive: Number,
        fifty: Number,
        hundred: Number,
        twohundred: Number,
        fivehundred: Number,
        thousand: Number
    },
    currentHand: {
        type: Array,
        required: true
    },
    hasPair: {
        type: Boolean,
        required: true,
        default: false
    },
    hasSplited: {
        type: Boolean,
        required: true,
        default: false
    },
    canDouble: {
        type: Boolean,
        required: true,
        default: false
    },
    dealerHand: {
        type: Array,
        required: true
    },
    deck: {
        type: Array,
    },
    currentHandValue: {
        type: Number,
    },
    dealerHandValue: {
        type: Number,
        required: true
    },
    splitHandValues:{
        type: Array
    },
    handHasBlackjack:{
        type: Array
    },
    handCanDouble:{
        type: Array
    },
    handWon:{
        type: Array
    },
    handTie:{
        type: Array
    },
    userIsBusted: {
        type: Boolean,
        required: true,
        default: false
    },
    userHasBlackjack: {
        type: Boolean,
        required: true,
        default: false
    },
    dealerHasBlackjack: {
        type: Boolean,
        required: true,
        default: false
    },
    handIsBusted:{
        type: Array
    },
    handStand:{
        type: Array
    },
    dealerWon:{
        type: Boolean,
        required: true,
        default: false
    },
    tie:{
        type: Boolean,
        required: true,
        default: false
    },
    userWon:{
        type: Boolean,
        required: true,
        default: false
    },
    dealerIsBusted: {
        type: Boolean,
        required: true,
        default: false
    }
});