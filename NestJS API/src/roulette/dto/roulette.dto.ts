export class CreateRouletteDto {
    userId: string;
    color: string;
    number: number;
    createdAt: Date;
    isOdd: boolean;
    bet: number;
}