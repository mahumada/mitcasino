export interface RouletteBet {
    numbers: number;
    quarters: number;
    doubles: number;
    isOdd: boolean;
    color: string;
    row: number;
    column: number;
    lessThanEighteen: boolean;
    dozens: number;
    bet: number;
}