export interface Roulette {
    id: string;
    userId: string;
    color: string;
    number: number;
    createdAt: Date;
    isOdd: boolean;
    bet: number;
}