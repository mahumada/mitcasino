import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Body,
    Param,
    Res,
  } from '@nestjs/common';
import { Roulette } from './interfaces/roulette.interface';
import { RouletteService } from './roulette.service';

@Controller('api/roulette')
export class RouletteController {
  constructor(private readonly rouletteService: RouletteService) {}
  @Get()
  async findAll(): Promise<Roulette[]>{
    return await this.rouletteService.findAll();
  }
    
}
