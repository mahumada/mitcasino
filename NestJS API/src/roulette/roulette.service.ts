import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/users/interfaces/user.interface';
import { Roulette } from './interfaces/roulette.interface';

@Injectable()
export class RouletteService {
    constructor(@InjectModel('Roulette') private readonly roulette: Model<Roulette>, @InjectModel('User') private readonly users: Model<User>) {}
    async findAll(): Promise<Roulette[]> {
        return await this.roulette.find();
    }
}
