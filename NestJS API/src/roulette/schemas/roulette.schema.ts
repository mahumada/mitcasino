import * as mongoose from 'mongoose';

export const RouletteSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    color: {
        type: String,
        default: null
    },
    number: {
        type: Number,
        default: null
    },
    createdAt:{
        type: Date,
        default: Date.now
    },
    isOdd: {
        type: Boolean,
        default: false
    },
    bet: {
        type: Number,
        required: true
    },
});

