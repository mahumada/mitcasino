import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateBJCoinsDto } from './dto/update-bjcoins.dto';
import { UsersService } from './users.service';
import { User } from './interfaces/user.interface';

const bcrypt = require('bcrypt');


@Controller('api/users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}
  @Get()
  async findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<User> {
    return this.usersService.findOne(id);
  }

  @Post()
  async create(@Body() createUserDto: CreateUserDto): Promise<User> {
    const encryptedPassword = await bcrypt.hash(createUserDto.password, 10);
    createUserDto.password = encryptedPassword;
    return this.usersService.create(createUserDto);
  }

  @Post('login')
  async login(@Body() body): Promise<boolean> {
    return this.usersService.login(body);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<User> {
    return this.usersService.delete(id);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() updateUserDto: CreateUserDto) {
    return this.usersService.update(id, updateUserDto);
  }

  @Put(':id/updateBJCoins')
  async updateBJCoins(@Param('id') id: string, @Body() updateBJCoinsDto: UpdateBJCoinsDto) {
    /*let body = {
      oneCoins: updateBJCoinsDto.oneCoins,
      fiveCoins: updateBJCoinsDto.fiveCoins,
      twentyFiveCoins: updateBJCoinsDto.twentyFiveCoins,
      hundredCoins: updateBJCoinsDto.hundredCoins,
      fiveHundredCoins: updateBJCoinsDto.fiveHundredCoins,
    } */
    return this.usersService.updateBJCoins(id, updateBJCoinsDto);
  }

}
