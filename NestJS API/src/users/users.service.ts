import { Injectable } from '@nestjs/common';
import { User } from './interfaces/user.interface';
import { BlackjackCoins } from './interfaces/bjcoins.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

const bcrypt = require('bcrypt');


@Injectable()
export class UsersService {
    constructor(@InjectModel('User') private readonly users: Model<User>) {}
    
    async findAll(): Promise<User[]> {
        return await this.users.find();
    }

    async findOne(id: string): Promise<User> {
        return await this.users.findOne({ _id: id });
    }

    async create(user: User): Promise<any> {
        return await this.users.findOne({username: user.username}).then( async (result) => {
            if (result) {
                return {error: "Username already exists"};
            } else {
                return await this.users.create(user);
            }
        })
    }

    async delete(id: string): Promise<User> {
        id = id.slice(0, -1);
        return await this.users.findByIdAndRemove(id);
    }

    async update(id: string, user: User): Promise<User> {
        return await this.users.findByIdAndUpdate(id, user, { new: true });
    }

    async login(body): Promise<any> {
        console.log(body);
        const user: User = await this.users.findOne({ username: body.username });
        const isValidPassword = await bcrypt.compare(body.password, user.password);
        if (!isValidPassword) {
            return {error: "Password or Username is incorrect"};
        }
        return user;
    }

    async updateBJCoins(id: string, bjCoins: BlackjackCoins): Promise<User> {
        return await this.users.findByIdAndUpdate(id, { blackJackCoins: bjCoins }, { new: true });
    }
    
    async deleteBJCoins(id: string): Promise<User> {
        return await this.users.findByIdAndUpdate(id, { blackJackCoins: null }, { new: true });
    }
}
