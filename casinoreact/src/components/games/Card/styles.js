import styled from 'styled-components';
import * as colors from '../../../utils/colors/index.js';

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: absolute;

  & .flipped {
    transition: .5s;
    transform: scaleX(1);
    width: 10%;
    position: absolute;
    left: 45%;
  }

  & .unflipped {
    transition: .5s;
    transform: scaleX(0);
    position: absolute;
    width: 10%;
    left: 45%;
  }
`

export const Image = styled.img`
  transition: 5s;
  width: 100%;
  position: absolute;
  top: 60%;
  left: 0;
`