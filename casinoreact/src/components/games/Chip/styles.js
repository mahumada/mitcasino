import styled from 'styled-components';

export const Wrapper = styled.img`
  width: 6%;
  position: absolute;
  @media (max-width: 650px){
    display: none;
  }
`

export const Info = styled.p`
  position: absolute;
  color: white;
  text-align: center;
  font-size: 1.2rem;
`