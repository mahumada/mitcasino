const standBJ = (userId) => {
  const body = {
    userId
  }
  return fetch(`http://localhost:5000/api/blackjack/stand`, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(err => console.error(err))
}

export default standBJ;