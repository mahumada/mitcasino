const createUser = (username, password, firstName, lastName, email, phone) => {
  const body = {
    username,
    password,
    firstName,
    lastName,
    email,
    phone
  }
  return fetch(`http://localhost:5000/api/users`, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(err => console.error(err))
}

export default createUser;