const deleteUser = id => {
  id += 'a';
  return fetch(`http://localhost:5000/api/users/${id}`, {
    method: 'DELETE'
  })
  .then(res => JSON.stringify(res))
  .catch(err => console.error(err));
}

export default deleteUser;