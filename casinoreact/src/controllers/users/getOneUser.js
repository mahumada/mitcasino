const getOneUser = id => {
  return fetch(`http://localhost:5000/api/users/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(err => console.error(err));
}

export default getOneUser;