import React from 'react';
import { Link } from 'react-router-dom';

function Error() {
  return (
    <div>
      <h1>ERROR 404: Page not found</h1>
      <Link to="/">Go home</Link>
    </div>
  )
}

export default Error;